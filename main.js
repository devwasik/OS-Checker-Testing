const { app, BrowserWindow } = require('electron')
const url = require('url')
const path = require('path')
var cmd = require('node-cmd');
var fs = require('fs');
var nodemailer = require('nodemailer');


let win

function createWindow() {
    win = new BrowserWindow({ width: 800, height: 600 })
    win.loadURL("google.com")

    //Run cmd commands
    cmd.get(
        'ping google.com',
        function(err, data, stderr) {
            console.log(data);
        }
    );

    //clear IE cache
    let cookies = cmd.get(

        "RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 8 " +
        "erase '%LOCALAPPDATA%\\Microsoft\\Windows\\Tempor~1\\*.*' /f /s /q " +
        "for /D %%i in ('%LOCALAPPDATA%\\Microsoft\\Windows\\Tempor~1\\*') do RD /S /Q '%%i'",
        function(err, data, stderr) {
            console.log(data);
            if (err) {
                console.log(err)
            }
        }
    );

    //  console.log(info);
}

app.on('ready', createWindow)


//Check diskspace
var diskspace = require('diskspace');
diskspace.check('C', function(err, result) {
    console.log(result.total);
});


//Check if file / programs exist
let check = fs.existsSync('c:\\users\\nick\\desktop\\weatherapp');
console.log(check);

//Check internet connection
require('dns').resolve('www.google.com', function(err) {
    if (err) {
        console.log("No connection");
    } else {
        console.log("Connected");
    }
});

//Check os stuffs
const os = require('os');
//free memory
console.log('free memory -> ' + os.freemem())
    //total memory
console.log('total memory -> ' + os.totalmem())

//uptime
console.log('uptime -> ' + os.uptime() / 60);

//user info
console.log('user info ->' + os.userInfo().username)
console.log('user info ->' + os.userInfo().username)


//EMAIL 


//https://docs.microsoft.com/en-us/outlook/rest/node-tutorial

var transporter = nodemailer.createTransport({
    host: 'smtp.office365.com', // Office 365 server
    port: 587, // secure SMTP
    secure: false, // false for TLS - as a boolean not string - but the default is false so just remove this completely
    auth: {
        user: 'nicholaswasik@northwesternmutual.com',
        pass: ''
    },
    tls: {
        ciphers: 'SSLv3'
    }
});
let mailOptions = {
    from: '"Nicholas Wasik" <nicholaswasik@northwesternmutual.com>', // sender address
    to: 'nickwasik@gmail.com', // list of receivers
    subject: 'Test Message', // Subject line
    text: 'Test', // plain text body
    html: 'Test' // html body
};
transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
});